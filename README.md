# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Install csqsh to communicate with cassandra 
	
	pip install cqlsh

### Start minikube with this configuration

	minikube start --memory 5120 --cpus=4	
	
*	You must save at least 1 CPU for your host
* 	If your host dont have enough CPU, you cans config 2 or 3 CPU for minicube 

### Get IP of Minicube
	
	minikube ip

### Open cassandra-service.yaml and set extenal ip with minikube ip

	externalIPs:
  	- <MINIKUBE-IP>

### Open application.properties in Spring project and set contact-points with minikube ip

	spring.data.cassandra.contact-points=<MINIKUBE-IP>
	
### Apply 2 yaml file:
	
	kubectl apply -f cassandra-service.yaml
	
	kubectl apply -f cassandra-stateful.yaml
	
### Test connection with cassandra with cqlsh

	cqlsh  <MINIKUBE-IP> 9042
	
### Create a test Datenbank with cqlsh shell

	create keyspace bezkoder with replication={'class':'SimpleStrategy', 'replication_factor':1};
	

	use bezkoder;
 
	
	CREATE TABLE tutorial(
	   id timeuuid PRIMARY KEY,
	   title text,
	   description text,
	   published boolean
	);
	
	
	CREATE CUSTOM INDEX idx_title ON bezkoder.tutorial (title) 
	USING 'org.apache.cassandra.index.sasi.SASIIndex' 
	WITH OPTIONS = {
	'mode': 'CONTAINS', 
	'analyzer_class': 'org.apache.cassandra.index.sasi.analyzer.NonTokenizingAnalyzer', 
	'case_sensitive': 'false'};


### Test if spring can connect with cassandra cluster:

	[POST] http://localhost:8080/api/tutorials
	
	JSON-raw:
	
	{
		"title": "Spring coures"
		"description": "Tut#3"
	}
	
	[GET] http://localhost:8080/api/tutorials
	
	
### References

*	https://kubernetes.io/docs/tutorials/stateful-application/cassandra/
*	https://bezkoder.com/spring-boot-cassandra-crud/


### Just to save data at localhost
minikube mount /Users/TheKietDang/Documents/cass/db:/home/docker/mnt/cass/db

minikube mount /home/kiet/Documents/kube-cass-spring/cass/db:/home/docker/mnt/cass/db



